"use strict";

var path = require('path');
var h = require('./helpers');
var t = h.assert;
var gw = require('../');

describe('gw', function () {

  it('default paths', function () {
    gw.load({});
    t.equal(gw.root, '/');
    t.equal(gw.enabledLocker, '/etc/gw/.enabled');
  });

  it('paths with custom root', function () {
    gw.load({root: __dirname});
    t.equal(gw.root, __dirname);
    t.equal(gw.enabledLocker, path.resolve(__dirname, './etc/gw/.enabled'));
  });
});