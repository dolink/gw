"use strict";

var path = require('path');
var fs = require('fs-extra');
var uuid = require('uuid');
var chai = require('chai');
chai.config.includeStack = true;
var superdir = require('./superdir');

var __slice = Array.prototype.slice;

var h = module.exports = {};
var t = h.assert = chai.assert;

h.tmplocal = function () {
  var args = __slice.call(arguments);
  return path.join.apply(path, [__dirname, 'tmp'].concat(args));
};

h.fixtures = function () {
  var args = __slice.call(arguments);
  return path.join.apply(path, [__dirname, 'fixtures'].concat(args));
};

h.clear = function () {
  fs.removeSync(h.tmplocal());
};

after(function () {
  h.clear();
});


h.dir = function (location) {
  return new superdir(location);
};

h.tmpdir = function () {
  return h.dir(path.join(h.tmplocal(), uuid.v4()));
};