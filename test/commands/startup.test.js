"use strict";

var path = require('path');
var fs = require('fs-extra');
var h = require('../helpers');
var t = h.assert;
var gw = require('../../');

describe('command/startup', function () {

  var root;

  before(function () {
    root = h.tmpdir();
  });

  beforeEach(function () {
    root.shadow(h.fixtures('raspbian'));
    gw.load({root: root.path});
  });

  it('should work', function () {
    return gw.commands.startup().then(function () {
      t.isTrue(root.exists('etc/gw'));
    });
  });
});