"use strict";

var _ = require('lodash');
var path = require('path');
var fs = require('fs-extra');
var h = require('../helpers');
var t = h.assert;
var gw = require('../../');

describe('command/serial', function () {

  var root, opts;

  before(function () {
    root = h.tmpdir();
  });

  beforeEach(function () {
    root.shadow(h.fixtures('raspbian'));
    opts = {root: root.path};
  });

  it('should work', function () {
    return gw.commands.serial(opts).then(function (serial) {
      t.equal(serial, '000000009A721DF8');
    });
  });

  it('should save serial to file with --save flag', function () {
    return gw.commands.serial(_.assign({}, opts, {save: true})).then(function () {
      t.equal(fs.readFileSync(path.resolve(gw.config['serial'])), '000000009A721DF8');
    });
  });
});
