"use strict";

var h = require('../helpers');
var t = h.assert;
var gw = require('../../');

describe('command/disable', function () {

  var rootdir;

  before(function () {
    rootdir = h.tmpdir();
  });

  beforeEach(function () {
    rootdir.shadow(h.fixtures('raspbian'));
    gw.load({root: rootdir.path});
  });

  it('should work', function () {
    return gw.commands.enable().then(function () {
      t.isTrue(rootdir.exists('boot/cmdline.txt.bak'));
      t.isTrue(rootdir.exists('etc/inittab.bak'));
      t.isTrue(rootdir.exists('etc/gw/.enabled'));
      return gw.commands.disable().then(function () {
        t.isFalse(rootdir.exists('boot/cmdline.txt.bak'));
        t.isFalse(rootdir.exists('etc/inittab.bak'));
        t.isFalse(rootdir.exists('etc/gw/.enabled'));
      });
    });
  });
});