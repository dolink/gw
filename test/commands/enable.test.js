"use strict";

var h = require('../helpers');
var t = h.assert;
var gw = require('../../');

describe('command/enable', function () {

  var rootdir;

  beforeEach(function () {
    rootdir = h.tmpdir();
    rootdir.shadow(h.fixtures('raspbian'));
    gw.load({root: rootdir.path});
  });

  it('should work', function () {
    t.isFalse(rootdir.exists('boot/cmdline.txt.bak'));
    t.isFalse(rootdir.exists('etc/inittab.bak'));
    t.isFalse(rootdir.exists('etc/gw/.enabled'));
    return gw.commands.enable().then(function () {
      t.isTrue(rootdir.exists('boot/cmdline.txt.bak'));
      t.isTrue(rootdir.exists('etc/inittab.bak'));
      t.isTrue(rootdir.exists('etc/gw/.enabled'));
    });

  });
});