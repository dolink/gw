"use strict";

var path = require('path');
var sh = require('../lib/utils/sh');
var h = require('./helpers');
var t = h.assert;
var pms = require('../lib/pms');
var npd = require('../lib/npd');

sh.config.silent = true;

describe('pms', function () {

  describe('#load()', function () {
    it('should load json with default local root', function () {
      var service = h.tmpdir().clear();
      service.write('service.json', {
        "app": {
          "script": './run.js',
          "args": {
            "--kitten": "cute"
          },
          "env": {
            "NODE_ENV": "development"
          }
        },
        "env": {
          "NODE_ENV": "production",
          "AWESOME_SERVICE_API_TOKEN": "xxx"
        },
        "args": {
          "--batman": "greatest-detective"
        }
      });

      var expected = [
        {
          "name": "app",
          "script": path.resolve(npd.localDir, 'app', 'run.js'),
          "args": "[\"--batman\",\"greatest-detective\",\"--kitten\",\"cute\"]",
          "env": {
            "AWESOME_SERVICE_API_TOKEN": "xxx",
            "NODE_ENV": "development"
          }
        }
      ];

      t.deepEqual(pms.load(path.join(service.path, 'service.json')), expected);
    });

    it('should load json with global root and custom module', function () {
      var service = h.tmpdir().clear();
      service.write('service.json', {
        "app": {
          "module": "haha",
          "script": './run.js',
          "args": {
            "--kitten": "cute"
          },
          "env": {
            "NODE_ENV": "development"
          }
        },
        "app2": {},
        "env": {
          "NODE_ENV": "production",
          "AWESOME_SERVICE_API_TOKEN": "xxx"
        },
        "args": {
          "--batman": "greatest-detective"
        }
      });

      var expected = [
        {
          "name": "app",
          "module": "haha",
          "script": path.resolve(npd.globalDir, 'haha', 'run.js'),
          "args": "[\"--batman\",\"greatest-detective\",\"--kitten\",\"cute\"]",
          "env": {
            "AWESOME_SERVICE_API_TOKEN": "xxx",
            "NODE_ENV": "development"
          }
        },
        {
          "name": "app2",
          "args": "[\"--batman\",\"greatest-detective\"]",
          "env": {
            "AWESOME_SERVICE_API_TOKEN": "xxx",
            "NODE_ENV": "production"
          }
        }
      ];

      t.deepEqual(pms.load(path.join(service.path, 'service.json'), {root: npd.globalDir}), expected);
    });
  });
});
