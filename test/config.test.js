"use strict";

var h = require('./helpers');
var t = h.assert;

var config = require('../lib/config');

describe('config', function () {

  it('should work', function () {
    var conf = config();
    t.notInclude(conf['enabled-locker'], '${');
  });
});