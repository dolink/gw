var assert = require('chai').assert;
var path = require('path');
var npd = require('../lib/npd');

describe('npd', function () {

  it('should work', function () {
    assert.equal(npd.globalDir, '/usr/local/silo');
    assert.ok(npd.localDir, path.resolve(__dirname, '../modules'));
  });
});