"use strict";

var path = require('path');
var fs = require('fs-extra');

module.exports = function (location) {
  return new Dir(location);
};

function Dir(location) {
  this.path = path.resolve(location);
}

Dir.prototype.shadow = function (src) {
  this.clear();
  fs.copySync(path.resolve(src), this.path);
  return this;
};

Dir.prototype.clear = function () {
  fs.removeSync(this.path);
  fs.mkdirpSync(this.path);
  return this;
};

Dir.prototype.glob = function (pattern) {
  return glob.sync(pattern, {
    cwd: this.path,
    dot: true
  });
};

Dir.prototype.write = function (filepath, contents) {
  if (typeof contents === 'object') {
    contents = JSON.stringify(contents, null, ' ') + '\n';
  }

  var fullPath = path.join(this.path, filepath);
  fs.mkdirpSync(path.dirname(fullPath));
  return fs.writeFileSync(fullPath, contents, 'utf-8');
};

Dir.prototype.read = function (name) {
  return fs.readFileSync(path.join(this.path, name), 'utf-8');
};

Dir.prototype.exists = function (name) {
  return fs.existsSync(path.join(this.path, name));
};

Dir.prototype.remove = function (name) {
  return fs.removeSync(path.join(this.path, name));
};