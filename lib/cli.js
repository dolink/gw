"use strict";

var _ = require('lodash');
var nomnom = require('nomnom');
var gw = require('./gw');
var pkg = require('../package.json');

var __slice = Array.prototype.slice;

module.exports = function (args) {
  args = args || [];

  var parser = nomnom()
    .script('gw')
    .option('version', {
      abbr: 'v',
      flag: true,
      help: 'show version.',
      callback: function () {
        return pkg.version;
      }
    })
    .option('uid', {
      abbr: 'u',
      metavar: 'UID',
      help: 'what user should scripts be executed as?'
    })
    .option('gid', {
      abbr: 'g',
      metavar: 'GID',
      help: 'what group should scripts be executed as?'
    });

  _.forEach(gw.commands, function (command, name) {
    var def = parser.command(name);

    if (command.options) {
      def.options(command.options);
    }
    if (command.help) {
      def.help(command.help);
    }
    if (command.usage) {
      def.usage(command.usage);
    }
    def.callback(function (argv) {
      if (_.isEmpty(argv.uid)) delete argv.uid;
      if (_.isEmpty(argv.gid)) delete argv.gid;

      gw.load(argv);
      var result = command.line(__slice.call(argv._, 1));
      if (result && result.catch) {
        result.catch(function (err) {
          console.error(err);
          process.exit(1);
        });
      } else if (_.isNumber(result) && result !== 0) {
        parser.print(parser.getUsage());
      }
    });
  });

  parser.command(null)
    .callback(function () {
      parser.print(parser.getUsage());
    });

  parser.parse(args);
};

function checkroot(cmd) {
  if (process.getuid() != 0) {
    console.log('`' + cmd + '` must be run as root');
    process.exit(1);
  }
}