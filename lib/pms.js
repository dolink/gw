"use strict";

var _ = require('lodash');
var path = require('path');
var fs = require('fs-extra');
var util = require('util');
var Promise = require('bluebird');
var osenv = require('osenv');
var sh = require('./utils/sh');

var log = require('wide');

var pm2 = sh.which('pm2');

exports.start = wrap('start');
exports.stop = wrap('stop');
exports.delete = wrap('delete');
exports.restart = wrap('restart');

function wrap(cmd) {
  return function (pathOrJson, opts) {
    opts = opts || {};
    var apps = load(pathOrJson, opts);
    var tmpdir = path.resolve(osenv.tmpdir(), Date.now().toString());
    var file = path.resolve(tmpdir, Date.now().toString() + '.json');
    fs.mkdirpSync(tmpdir);
    fs.writeJsonSync(file, apps);

    var command = opts.user ? 'su ' + opts.user + ' -c "%s"' : '%s';
    command = util.format(command, [pm2, cmd, file].join(' '));
    log.verbose('pm2', command);
    sh.execSync(command, {silent: opts.silent !== false});

    fs.removeSync(tmpdir);
    return Promise.resolve();
  };
}

function load(pathOrJson, opts) {
  opts = opts || {};
  var json = pathOrJson;
  if (_.isString(pathOrJson)) {
    json = fs.readJsonSync(path.resolve(pathOrJson));
  }
  return parse(json, opts);
}
exports.load = load;

//function build(json, opts) {
//    return parse(opts.data ? _tpl(json, opts.data, { mustache: true }) : json, opts);
//}

function parse(json, opts) {
  var root = opts.root || require('./npd').localDir;
  var apps = [];

  _.forEach(json, function (config, name) {
    if (name === 'args' || name === 'env') return;

    var app = _.assign({
      name: name
    }, config);

    var mod = app.module || app.name;
    if (mod && app.script) {
      app.script = path.resolve(root, mod, app.script);
    }

    if (json.env || config.env) {
      app.env = _.assign({}, json.env, config.env);
    }

    var args = config.args;

    if (_.isArray(config.args)) {
      if (_.isArray(json.args)) args = [].concat(json.args, args);
    } else {
      args = _.assign({}, json.args, config.args);
    }

    app.args = args;
    if (!_.isArray(args)) {
      app.args = [];
      _.forEach(args, function (v, k) {
        app.args.push(k);
        app.args.push(v);
      });
    }

    apps.push(app);
  });

  return apps;
}