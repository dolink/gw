"use strict";

var fs = require('fs-extra');
var sh = require('./sh');

module.exports = function (dir) {
  if (!dir || !fs.existsSync(dir)) {
    throw new Error('`' + dir + '` not exists');
  }
  var dir_bak = dir + '.bak';

  function isBackuped() {
    return fs.existsSync(dir_bak);
  }

  function backup(force) {
    if (!isBackuped() || force) {
      fs.copySync(dir,  dir_bak);
      return true;
    }
    return false;
  }

  function restore() {
    sh.execSync('mv ' + dir_bak + ' ' + dir);
  }

  return {
    isBackuped: isBackuped,
    backup: backup,
    restore: restore
  };
};
