"use strict";

var _ = require('lodash');
var path = require('path');
var Promise = require('bluebird');
var shell = require('shelljs');

var winBatchExtensions;
var winWhichCache;
var isWin = process.platform === 'win32';

if (isWin) {
  winBatchExtensions = ['.bat', '.cmd'];
  winWhichCache = {};
}

shell.config.silent = true;

_.assign(exports, shell);
exports.execSync = function (/*cmd, args, opts*/) {
  var cmd, args, opts, i, a;
  for (i = 0; i < arguments.length; i++) {
    a = arguments[i];
    if (!cmd && _.isString(a)) {
      cmd = a;
    } else if (!args && (_.isArray(a) || _.isString(a))) {
      args = a;
    } else if (!opts && _.isObject(a)) {
      opts = a;
    }
  }
  args = args || [];
  if (!Array.isArray(args)) args = [args];
  args.unshift(cmd);
  var command = args.join(' ');
  opts = opts || {};
  opts.async = false;

  var result;
  var owd = process.cwd();

  if (opts.cwd) shell.cd(opts.cwd);
  result = shell.exec(command, opts);
  if (opts.cwd) shell.cd(owd);

  if (result.code !== 0) {
    throw new Error(result.output);
  }
  return result.output;
};

exports.execAsync = function (/*cmd, args, opts*/) {
  var cmd, args, opts, i, a;
  for (i = 0; i < arguments.length; i++) {
    a = arguments[i];
    if (!cmd && _.isString(a)) {
      cmd = a;
    } else if (!args && (_.isArray(a) || _.isString(a))) {
      args = a;
    } else if (!opts && _.isObject(a)) {
      opts = a;
    }
  }
  args = args || [];
  if (!Array.isArray(args)) args = [args];
  args.unshift(cmd);
  var command = args.join(' ');
  opts = opts || {};
  opts.async = true;

  return new Promise(function (resolve, reject) {
    shell.exec(command, opts, function (code, output) {
      if (code) return reject(output);
      return resolve(output);
    });
  });
};

