"use strict";

var _ = require('lodash');
var path = require('path');
var fs = require('fs-extra');
var log = require('wide');
var gwconf = require('./gwconf');
var backer = require('./utils/backer');

var pkg = require('../package.json');

var gw = module.exports = {};

var _config;
gw.load = function (opts) {
  if (opts || !_config) {
    _load(opts);
  }
};

function _load(opts) {
  _config = gwconf(opts);

  gw._backers = null;

  log.level = _config["loglevel"] || "info";
  log.cli('gw', {appversion: pkg.version, timestamp: 'short'});
}

Object.defineProperty(gw, 'config', {
  get: function () {
    if (!_config) throw new Error('gw.load() required');
    return _config;
  }
});

Object.defineProperty(gw, 'dir', {
  get: function () {
    return path.resolve(__dirname, '..');
  }
});

Object.defineProperty(gw, 'root', {
  get: function () {
    return gw.config['root'];
  }
});

Object.defineProperty(gw, 'enabledLocker', {
  get: function () {
    return path.resolve(gw.root, gw.config['enabled-locker']);
  }
});

Object.defineProperty(gw, 'inittab', {
  get: function () {
    return path.resolve(gw.root, gw.config['inittab']);
  }
});

Object.defineProperty(gw, 'cmdline', {
  get: function () {
    return path.resolve(gw.root, gw.config['cmdline']);
  }
});

Object.defineProperty(gw, 'backers', {
  get: function () {
    if (!gw._backers) {
      gw._backers = {
        inittab: backer(gw.inittab),
        cmdline: backer(gw.cmdline)
      }
    }
    return gw._backers;
  }
});


Object.defineProperty(gw, 'enabled', {
  get: function () {
    return fs.existsSync(gw.enabledLocker);
  }
});

Object.defineProperty(gw, 'commands', {
  get: function () {
    return require('./commands');
  }
});

