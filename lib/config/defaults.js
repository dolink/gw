"use strict";

var path = require('path');
var osenv = require('osenv');

module.exports = function () {
  return {
    "uid": osenv.user(),
    "root": '/',
    "resetpin": 16,
    "cfg": "${root}/etc/gw",
    "inittab": '${root}/etc/inittab',
    "cmdline": '${root}/boot/cmdline.txt',
    "enabled-locker": '${cfg}/.enabled',
    "serial": '${cfg}/serial.conf',
    "serviceJsonPath": path.resolve(__dirname, '../../service.json')
  };
};