"use strict";

var rc = require('rc');
var ini = require('ini');
var properties = require('properties');
var defaults = require('./defaults');

module.exports = function (opts) {
  var conf = rc('gw', defaults(), opts);
  var encoded = ini.encode(conf);
  conf = properties.parse(encoded, {variables: true});
  return conf;
};
