"use strict";

var Promise = require('bluebird');
var fs = require('fs-extra');
var sh = require('../utils/sh');
var gw = require('../gw');

var disable = require('./disable');

enable.help = 'enable gateway configuration';
enable.options = {
  force: {
    abbr: 'f',
    flag: true,
    help: 'force enable'
  }
};

function enable(opts) {
  gw.load(opts);
  opts = gw.config;

  if (gw.enabled && !opts.force) {
    return console.log('IGNORE! Gateway has been enabled');
  }

  if (opts.force) disable();

  console.log('Enabling gateway configuration...');

  if (gw.backers.inittab.backup()) {
    sh.sed('-i', /.*ttyAMA0.*/, '#$&', gw.inittab);
  }

  if (gw.backers.cmdline.backup()) {
    sh.sed('-i', /console=ttyAMA0,115200/, '', gw.cmdline);
    sh.sed('-i', /kgdboc=ttyAMA0,115200/, '', gw.cmdline);
  }

  fs.createFileSync(gw.enabledLocker);

  return Promise.resolve();
}

enable.line = function () {
  return enable().then(function () {
    console.log('Done! Please reboot to apply configuration.');
  });
};

module.exports = enable;

