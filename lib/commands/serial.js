"use strict";

var Promise = require('bluebird');
var path = require('path');
var fs = require('fs-extra');
var sh = require('../utils/sh');
var gw = require('../gw');

serial.help = 'generate gateway serial';
serial.options = {
  save: {
    abbr: 's',
    flag: true,
    help: 'save serial to serial file'
  }
};

function serial(opts) {
  gw.load(opts);
  opts = gw.config;
  var cpuinfo = path.resolve(opts.root, 'proc/cpuinfo');
  if (!fs.existsSync(cpuinfo)) {
    return Promise.reject(new Error('Can not find ' + cpuinfo + ' file'));
  }
  var line = sh.grep('Serial', cpuinfo);
  var values = line.split(':');
  if (!values || !values.length || values.length < 2) {
    return Promise.reject(new Error('Can not find property `Serial` in proc/cpuinfo'));
  }
  var serial = values[1].trim().toUpperCase();

  if (opts.save) {
    var file = path.resolve(opts['serial']);
    var dir = path.dirname(file);
    fs.mkdirpSync(dir);
    fs.writeFileSync(file, serial, 'utf8');
  }

  console.log(serial);
  return Promise.resolve(serial);
}

serial.line = function () {
  return serial();
};

module.exports = serial;

