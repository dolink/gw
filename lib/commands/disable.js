"use strict";

var Promise = require('bluebird');
var fs = require('fs-extra');
var gw = require('../gw');

disable.help = 'restoring origin configuration';

function disable() {
  if (gw.enabled) {
    console.log("Restoring your old configuration...");
    gw.backers.inittab.restore();
    gw.backers.cmdline.restore();
    fs.removeSync(gw.enabledLocker);
  }
  return Promise.resolve(!!gw.enabled);
}

disable.line = function () {
  return disable().then(function (value) {
    if (value) {
      console.log("Done! Please reboot to restore old configuration.");
    } else {
      console.warn("Didn't find an old configuration. Are you sure you've enabled before?");
    }
    return value;
  });
};

module.exports = disable;



