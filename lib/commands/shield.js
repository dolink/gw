"use strict";

var Promise = require('bluebird');
var path = require('path');
var log = require('wide');
var sh = require('../utils/sh');
var gw = require('../gw');

var __slice = Array.prototype.slice;

module.exports = shield;

shield.help = 'shield detect|version|flash';
shield.usage = 'Usage: gw shield <cmd> [<opts>]'
  + '\ngw shield detect'
  + '\ngw shield version'
  + '\ngw shield flash <file>';

function shield(targets, opts) {
  var act = targets[0];

  if (!act) return 1;
  if (!shield[act]) return 2;
  gw.load(opts);

  return shield[act](__slice.call(targets, 1));
}

shield.line = function (targets) {
  return shield(targets);
};

shield.detect = function detect() {
  var gpio = Promise.promisifyAll(require("pi-gpio"));

  var pin = gw.config.resetpin;
  return gpio.exportAsync(pin, "in down")
    .then(function () {
      return gpio.readAsync(pin);
    })
    .then(function (value) {
      if (value) {
        console.log('Shield is installed');
      } else {
        console.log('Shield not installed');
      }
    })
    .finally(function () {
      gpio.closeAsync(pin);
    });
};

shield.flash = function flash(targets) {
  if (!targets || !targets.length) return log.error('shield-flash', 'expects hex file');
  var target = targets[0];
  target = path.resolve(process.cwd(), target);

  var cmd = 'avrdude';
  var args = ['-p', 'm644p', '-c', 'arduino', '-P', '/dev/ttyAMA0', '-b', '115200', '-D', '-U', 'flash:w:' + target + ':i'];
  log.info('shield-flash', 'Flashing shield...');

  return sh.execAsync(cmd, args, {silent: false})
    .then(function () {
      log.info('shield-flash', 'Done.');
    });
};