var Promise = require('bluebird');
var path = require('path');
var fs = require('fs-extra');
var log = require('wide');
var Handlebars = require('handlebars');
var sh = require('../utils/sh');
var gw = require('../gw');

startup.help = 'auto resurrect gateway processes at startup';

function startup(opts) {
  gw.load(opts);
  opts = gw.config;
  var uid = opts.uid;

  var ETC_DIR = path.resolve(gw.root, "etc/gw");
  var INIT_SCRIPT = path.resolve(gw.root, "etc/init/gw.conf");

  // create /etc/gw dir
  fs.mkdirpSync(ETC_DIR);
  if (uid) {
    sh.execSync('chown -R ' + uid + ' ' + ETC_DIR);
  }


  var data = {
    GW_PATH: process.mainModule.filename,
    HOME_PATH: (process.env.GW_HOME || process.env.HOME),
    BIN_PATH: path.dirname(process.execPath),
    USER: uid
  };

  var script = fs.readFileSync(path.join(__dirname, '../scripts/gw.conf'), 'utf-8');
  var template = Handlebars.compile(script);
  script = template(data);

  log.info('startup', 'Generating system init script in ' + INIT_SCRIPT);
  fs.mkdirpSync(path.dirname(INIT_SCRIPT));
  fs.writeFileSync(INIT_SCRIPT, script/*, { mode: 0755 }*/);

  if (!fs.existsSync(INIT_SCRIPT)) {
    log.error(' There is a problem when trying to write file : ' + INIT_SCRIPT);
    return Promise.reject({msg: 'Problem with ' + INIT_SCRIPT});
  }

  log.info('startup', 'Generated: ' + INIT_SCRIPT);
  return Promise.resolve();
}

startup.line = function () {
  //if (process.getuid() !== 0) {
  //    console.log("You must run this script as root");
  //    process.exit(1);
  //}
  return startup();
};

module.exports = startup;

