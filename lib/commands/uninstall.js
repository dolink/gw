"use strict";

var path = require('path');
var fs = require('fs-extra');
var log = require('wide');
var Promise = require('bluebird');
var gw = require('../gw');
var pms = require('../pms');

var stop = require('./stop');

uninstall.help = 'uninstall gateway apps';

function uninstall() {
  gw.load();
  var c = gw.config;
  return Promise.resolve()
    .then(function () {
      log.action('uninstall', 'stopping services');
      return stop();
    })
    .then(function () {
      var INIT_SCRIPT = path.resolve(gw.root, "etc/init/gw.conf");
      log.action('uninstall', 'remove', INIT_SCRIPT);
      fs.removeSync(INIT_SCRIPT);
    });
}

uninstall.line = function () {
  return uninstall();
};

module.exports = uninstall;