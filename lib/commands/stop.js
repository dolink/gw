"use strict";

var gw = require('../gw');
var pms = require('../pms');

stop.help = 'stop gateway apps';

function stop() {
  gw.load();
  return pms.delete(gw.config.serviceJsonPath, {root: require('../npd').globalDir, silent: false});
}

stop.line = function () {
  return stop();
};

module.exports = stop;

