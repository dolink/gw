"use strict";

var path = require('path');
var Promose = require('bluebird');
var gw = require('../gw');

install.help = 'install gateway components';

function install(opts) {
  gw.load(opts);
  return Promose.resolve()
    .then(gw.commands.uninstall)
    .then(gw.commands.enable)
    .then(gw.commands.startup);
}

install.line = function () {
  return install();
};

module.exports = install;