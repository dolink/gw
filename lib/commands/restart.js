"use strict";

var path = require('path');
var gw = require('../gw');
var pms = require('../pms');

restart.help = 'restart gateway apps';

function restart() {
  gw.load();
  return pms.restart(gw.config.serviceJsonPath, {root: require('../npd').globalDir, silent: false});
}

restart.line = function () {
  return restart();
};

module.exports = restart;

