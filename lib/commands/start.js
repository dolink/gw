"use strict";

var path = require('path');
var gw = require('../gw');
var pms = require('../pms');

start.help = 'start gateway apps';

function start() {
  gw.load();
  return pms.start(gw.config.serviceJsonPath, {root: require('../npd').globalDir, silent: false});
}

start.line = function () {
  return start();
};

module.exports = start;

